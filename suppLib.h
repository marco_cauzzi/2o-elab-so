#ifndef SUPPLIB_H
#define SUPPLIB_H
/**
 * @file suppLib.h
 * @author Marco Cauzzi
 * @date 28/06/16
 * @brief Contains globals, variables and function declarations
 *
 */
 
//Includes
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/sem.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/types.h>
#include <errno.h>
#include <sys/shm.h>
#include <signal.h>
/**
 * @brief Constants Declarations
 * 
 * @param BUF_LEN Defines the maximum length of the cfg in bytes
 * @param MAX_OPS Maximum number of processes
 * @param MAX_NUM_LEN Defines the maximum length of a number
 * @param SEM_KEY Key of the semaphore array
 * @param STDIN Defines the stdin file descriptor
 * @param STOUT Defines the stdout file descriptor
 * @param SEMGET_ERROR Defines the semaphore error message
 * @param CFG_FILE_ERROR Defines cfg file opening error message
 * @param SHM_MEM_ERROR Defines the shared memory creation error message
 */
//Constants 
#define BUF_LEN 1024
#define MAX_OPS 20
#define MAX_NUM_LEN 20
#define SEM_KEY 20
#define STDIN  0
#define STDOUT 1
#define SEMGET_ERROR "Errore nella creazione dei semafori errno=%d\n"
#define CFG_FILE_ERROR "Errore nell apertura del file di configurazione errno=%d\n"
#define SHM_MEM_ERROR "Errore nella creazione della memoria condivisa errno=%d\n"
#define OUT_FILE_ERROR "Errore nell'apertura del file di output\n" 
#define FORK_ERROR "Errore Nella fork\n"

//Structs 
/**
 * @brief Contains the PID and the shared memory id used by the father to communicate with the sons
 *  
 * 
 */
typedef struct processo{					
	int pid;			
	int shmId;			
}proc;
/**
 * @brief Contains all the parts of an operation and the operation number
 */
typedef struct operation{
	int op1;
	char operand;
	double op2;
	int opNum;
}op;
/**
 * @brief Union declaration for the semctl
 */
union semun {
   int              val;    /* Value for SETVAL */
   struct semid_ds *buf;    /* Buffer for IPC_STAT, IPC_SET */
   unsigned short  *array;  /* Array for GETALL, SETALL */
   struct seminfo  *__buf;  /* Buffer for IPC_INFO
							   (Linux-specific) */
}arg;
/**
 * @brief Global variables definitions
 * 
 * @param writeBuf Buffer used by the write function to write to stdout
 * @param sop
 * @param semId Contains the id of the semaphore array
 * @param results Contains all the operation's results
 * @param numProc Contains the number of processes
 * @param procs array containing the proc structure
 * @param shms Array containing the shared memories used by the father
 */
//Variables 
char writeBuf[BUF_LEN];
struct sembuf sop;
int semId;
double* results;
int numProc;
proc* procs; //List of processes created
op** shms;	//Shared memory segments used by the father

//Functions
//returns 1 if c is either *, /, + or -.
/**
 * @brief determines if c is an algebraic operator
 * @param c the character to analyze
 * @return Returns 1 if c is an operator, 0 otherwise
 */
int isOperator(char c);
/**
 * @brief determines if c is number
 * @param c the character to analyze
 * @return Returns 1 if c is a number, 0 otherwise
 */
int isNumber(char c);
/**
 * @brief determines if c is a valid character (operator or number)
 * @param c the character to analyze
 * @return Returns 1 if c is valid, 0 otherwise
 */
int validCheck(char c);
/**
 * @brief Executes the semOp on the semNum semaphore
 * @param semNum The number of the semaphore on which the function has to operate
 * @param semOp the operations to execute
 */
int execSemOp(int semNum, int semOp);
/**
 * @brief Executes the operations contained in oper
 * @param oper The struct containing the operation
 * @return Returns the result of the operation
 */
double getResult(op oper);
/**
 * @brief Close all child processes
 */
void closeAllProcesses();
/**
 * @brief Get the num at the line line and col column
 * @param ops the charArray on which we have to operate
 * @param line the line at which we have to search the number
 * @param col The column of the number
 * @return Returns the number
 */
int getNum(char** ops, int line, int* col);
/**
 * @brief Read the operations from the file
 * @param fd The file descriptor of the file
 * @param ops The charArray in which we store the the things we read
 * @return Returns the number of lines read
 */
int readOps(char** ops, int fd);
/**
 * @brief Wait for all child processes to initialize
 */
void waitForReady();
/**
 * @brief Search the process having the pid argument in the proc array
 * @param pid The pid that we have to search
 * @return Returns the id of the pid process
 */
int searchProcessByPid(int pid);
/**
 * @brief Close the semaphore array
 */
void closeSems();
/**
 * @brief Close the shared memory segments
 */
void closeShm();
#endif
