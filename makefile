# Sources:
SRCS:=main.c suppLib.c
OBJS:=$(SRCS:.c=.o)
HDRS:= suppLib.h
# Config:
CC:=gcc
CFLAGS:= -c -g
LD:=gcc
TT:=tar
TFLAGS:=cvzf
ZIPNAME:=MarcoCauzziVR376899_IPC
DOXYCFG:=Doxyfile
# Targets:

all: elab2 tar
clean:
	@echo Cleaning.
	@rm -f *.o
	@rm -f elab2

elab2: $(OBJS)
	@echo $@
	@$(LD) -o $@ $^
	
tar:
	@$(TT) -$(TFLAGS) $(ZIPNAME).tgz $(SRCS) $(HDRS) $(DOXYCFG) -C html .
	
%.o:%.c
	@echo $@
	@ $(CC) $(CFLAGS) -o $@ $<

.PHONY: all clean
