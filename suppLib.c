#include "suppLib.h"

int isOperator(char c){
	if(c=='+' || c=='/' || c=='-' ||c == '*' || c==' '){
		return 1;
	}
	return 0;
}

int isNumber(char c){
	if(c>='0' && c<='9'){
		return 1;
	}
	return 0;
}

 int validCheck(char c){
	if(isNumber(c) || isOperator(c)){
		return 1;
	}
	return 0; 
}

int execSemOp(int semNum,int semOp){
	sop.sem_num=semNum;
	sop.sem_op=semOp;
	semop(semId,&sop,1);
}

double getResult(op oper){
	switch (oper.operand){
		case '+':
			return oper.op1+oper.op2;
			break;
		case '-':
			return oper.op1-oper.op2;
			break;
		case '*':
			return oper.op1*oper.op2;
			break;
		case '/':
			return oper.op1/oper.op2;
			break;
		default:
			return 0;
	}
}

void closeAllProcesses(){
	int i;
	
	for(i=0;i<numProc;i++){
		shms[i]->operand='K';
	}
}

 int getNum(char** ops, int line, int* col){
	int i=line;
	int j=*col;
	int c=0;
	char num[MAX_NUM_LEN];
	
	while(isNumber(ops[i][j])){	
		num[c++]=ops[i][j++];
	}
	
	*col=j;
	return strtol(num,NULL,10);
 }
 
 int readOps(char** ops, int fd){
	char temp[BUF_LEN];
	int i,j,x; 
	int numRead=read(fd,temp,BUF_LEN);
	                                               
	for(i=0,j=0,x=0;i<numRead;i++){
		if(temp[i]=='\n'){
			 j++;
			 x=0;
		}else if(validCheck(temp[i])){
			ops[j][x++]=temp[i];
		} 
	}
	
	return j;
}

 void waitForReady(){
	 int i;
	 for(i=0;i<numProc;i++){
		 if(semctl(semId,i,GETVAL,arg)==0){
			i=0;
		}
	 }
 }
 
 int searchProcessByPid(int pid){
	 int i;
	 for(i=0;i<numProc;i++){
		 if(pid==procs[i].pid){
			 return i;
		 }
	 }
	 return -1;
 }

void closeSems(){
	semctl(semId,0,IPC_RMID,0);
}

void closeShm(){
	int i;
	for(i=0;i<numProc;i++){
		shmdt(shms[i]);
		shmctl(procs[i].shmId,IPC_RMID,NULL);
	}
}
