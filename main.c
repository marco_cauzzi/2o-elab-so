#include "suppLib.h"
 /**
 * @file main.c
 * @author Marco Cauzzi
 * @date 28/06/16
 * @brief Ccontains the main parts of the program (the child, the operation dispatcher and the main)
 */
 
 /**
 * @brief The child function, executes the operation passed to her and signals the father of its completion
 * 
 * @param id The id of the child
 */
 int childFunc(int id){
	int shmId;
	
	if ((shmId = shmget (getpid(), sizeof(op), 0666|IPC_CREAT)) == -1){
				sprintf(writeBuf,SHM_MEM_ERROR,errno);
				write(STDOUT,writeBuf,errno);
				return -1;
	}
	
	op* shm=shmat(shmId,NULL,0666);
	execSemOp(id,1);
	sprintf(writeBuf,"FIGLIO:Processo id=%d pronto\n",id);
	write(STDOUT,writeBuf,strlen(writeBuf));
	while(1){
		execSemOp(id,0);
		
		sprintf(writeBuf,"FIGLIO:Processo id %d ha ricevuto l'operazione %d %c %f\n",id,shm->op1,shm->operand,shm->op2);
		write(STDOUT,writeBuf,strlen(writeBuf));
		
		if(shm->operand=='K'){
			sprintf(writeBuf,"FIGLIO:Il Process id=%d è stato terminato\n",id);
			write(STDOUT,writeBuf,strlen(writeBuf));
			exit(0);
		}
		
		shm->op2=getResult(*shm);
		 
		kill(getppid(),SIGCONT);
		sprintf(writeBuf,"FIGLIO:Inviato segnale al padre %d\n",getppid());
		write(STDOUT,writeBuf,strlen(writeBuf));
		execSemOp(id,1);
	}
 }
  /**
 * @brief Dispatches the operations to the child processes
 * 
 * @param ops The charArray containing the operations
 * @param lineRead The total number of lines read
 */
 int sendToChildren(char** ops,int lineRead){
	 op oper;
	 int i;
	 int j=0;
	 int procId,id,sent;
	 
	 for(i=1;i<lineRead;i++){
		procId=getNum(ops,i,&j);
		j++;
		oper.op1=getNum(ops,i,&j);
		oper.operand=ops[i][j++];
		oper.op2=getNum(ops,i,&j);
		oper.opNum=i-1;;
		sprintf(writeBuf,"La prossima operazione %d è %d %c %f, trasmessa al processo %d\n",oper.opNum,oper.op1,oper.operand,oper.op2,procId);
		write(STDOUT,writeBuf,strlen(writeBuf));
		j=0;
		
		if(procId>0){
			if(semctl(semId,procId-1,GETVAL,arg)==0){
				while(semctl(semId,procId-1,GETVAL,arg)==0){
				}
			}
			*shms[procId-1]=oper;
			execSemOp(procId-1,-1);
		}else if(procId==0 ){
			sent=0;
			while(sent==0){
				for(id=0;id<numProc;id++){
					if(semctl(semId,id,GETVAL,arg)==1){
						*shms[id]=oper;
						execSemOp(id,-1);
						id=numProc;
						sent=1;
					}
				}
			}
		}
	}
}

 /**
 * @brief Saves the result of an operation in the results array
 * 
 * @param sig The id of the signal received by the child
 * @param si The struct ontaining info about the signal received
 */
 static void endCalcHandler(int sig,siginfo_t *si, void *unused){
	int procId;
	procId=searchProcessByPid(si->si_pid);
	sprintf(writeBuf,"Ricevuto segnale dal processo con pid=%d, risultato=%f\n",si->si_pid,shms[procId]->op2);
	write(STDOUT,writeBuf,strlen(writeBuf));
	results[shms[procId]->opNum]=shms[procId]->op2;
 }
 
int main(){
	int cfgDesc=open("config.cfg",O_RDONLY,S_IRUSR);
	int i,j;
	int outDesc=open("output.txt,",O_WRONLY|O_CREAT|O_TRUNC,0666);
	char* ops[MAX_OPS];
	struct sigaction sa;
	
	sa.sa_flags=SA_SIGINFO;
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = endCalcHandler;
	sigaction(SIGCONT,&sa,NULL);
	
	if(cfgDesc==-1){
		write(STDOUT,CFG_FILE_ERROR,strlen(CFG_FILE_ERROR));
		return -1;
	}
	
	if(outDesc==-1){
		write(STDOUT,OUT_FILE_ERROR,strlen(OUT_FILE_ERROR));
		return -1;
	}
	
	for(i=0;i<MAX_OPS;i++){
		ops[i]=malloc(BUF_LEN*sizeof(char));
	}
	
	int lineRead=readOps(ops,cfgDesc);
	
	for(i=0;i<lineRead;i++){
		sprintf(writeBuf,"%s \n",ops[i]);
		write(STDOUT,writeBuf, strlen(writeBuf));
	}
	
	int firstCol=0;
	numProc=getNum(ops,0,&firstCol);
	procs=malloc(numProc*sizeof(proc));
	results=malloc((lineRead-1)*sizeof(double));
	shms=malloc(numProc*sizeof(op*));
	for(i=0;i<numProc;i++){
		shms[i]=malloc(sizeof(op));
	}
	
	if((semId=semget(SEM_KEY,numProc+1,IPC_CREAT | IPC_EXCL| 0666))==-1){
		sprintf(writeBuf,SEMGET_ERROR,errno);
		write(STDOUT,writeBuf,strlen(writeBuf));
		return -1;
	}
	
	printf("Il semId è %d\n",semId);
	sprintf(writeBuf,"Numero di Processi=%d\n",numProc);
	write(STDOUT,writeBuf,strlen(writeBuf));
	
	for (i=0;i<numProc;i++){
		procs[i].pid=fork();
		if(procs[i].pid==-1){
			write(STDOUT,FORK_ERROR,strlen(FORK_ERROR));
		}
		if(procs[i].pid!=0){
			if ((procs[i].shmId = shmget (procs[i].pid, sizeof(op), 0777|IPC_CREAT)) == -1){
				sprintf(writeBuf,SHM_MEM_ERROR,errno);
				write(STDOUT,writeBuf,strlen(writeBuf));
				return -1;
			}
			shms[i]=shmat(procs[i].shmId,NULL,0666);
		}
		
		if(procs[i].pid==0){
			childFunc(i);
			return 0;
		}
		
	}
;	
	waitForReady();
	sendToChildren(ops,lineRead);
	
	waitForReady();
	
	for(i=0;i<lineRead-1;i++){
		sprintf(writeBuf,"Risultato operazione %d =%f\n",i+1,results[i]);
		write(outDesc,writeBuf,strlen(writeBuf));
	}
	closeAllProcesses();
	free(procs);
	free(results);
	free(shms);
	for(i=0;i<MAX_OPS;i++){
		free(ops[i]);
	}
	closeSems();
	closeShm();
	close(outDesc);
	while(wait(NULL)>0);
	return 0;
}
